# Login App
Login (Logistic Interactive) is a fuel management system mobile app powered by PT Harmoni Panca Utama. ©2022
 
## Manual Book 

[Logistic Interactive Manual Book](https://drive.google.com/file/d/122dAlw6Dbx95SP3VfKTWGWXqzWQvpoDI/view?usp=sharing)

## Installation
- Android min. v4 (min. v5 is recommended)
- Install apk 
- If there's pop up, choose 'Install anyway'
- Login by input NIK and password 

## Notes
- Skip the nullable data form field (no need to input anything)
- Use (.) for decimal number (e.g. 0.840)

 
