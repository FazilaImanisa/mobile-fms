part of '../pages.dart';

class HomeManual extends StatefulWidget {
  const HomeManual({Key? key}) : super(key: key);

  @override
  _HomeManualState createState() => _HomeManualState();
}

class _HomeManualState extends State<HomeManual> {
  late final int index;
  bool isChecked = false;
  List<Map> check = [];
  List <CheckboxValue> checkboxValue = [
    CheckboxValue(name: "tr_sounding", isSelected:false),
    CheckboxValue(name: "tr_fuel_transfer", isSelected:false),
    CheckboxValue(name: "tr_refueling", isSelected:false),
    CheckboxValue(name: "tr_baps", isSelected:false),
    CheckboxValue(name: "tr_fuel_attendance", isSelected:false),
  ];

  @override
  initState(){
    Global.getData().then((value) {
      setState(() {
        check = value;
      });
    });
  }


  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height/1.2,
      color: Colors.white,
      child: DefaultTabController(
        length: 2,
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            leading: new IconButton(
              icon: new Icon(
                Icons.arrow_back,
                color: Colors.black,
              ),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => homeDashboard()),
                );
              },
            ),
            title: const Text('Sinkronisasi',
                style: TextStyle(
                    color: Colors.black,
                    fontFamily: Fonts.REGULAR,
                    fontSize: 18)),

            backgroundColor: Colors.white,
            bottom: const TabBar(
              physics: NeverScrollableScrollPhysics(),
              indicatorColor: Coloring.mainColor,
              tabs: [
                Tab(
                  child: Text(
                      'Download',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Coloring.mainColor,
                          fontFamily: Fonts.REGULAR,fontSize: 12)
                  ),),
                Tab(
                  child: Text(
                      'Upload',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Coloring.mainColor,
                          fontFamily: Fonts.REGULAR,fontSize: 12)
                  ),),
              ],
            ),

          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children:  <Widget>[
              SingleChildScrollView(
                child: Column(
                  children: [
                    Container(
                      margin: EdgeInsets.symmetric(horizontal: 20,vertical: 20),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          InkWell(
                            onTap: () async{
                              LoadingBar.dialogLoading(context);

                              var downloadEquipment = await ApiService.downloadEquipment();
                              var downloadEmployee = await ApiService.downloadEmployee();
                              var downloadStorage = await ApiService.downloadStorage();
                              var downloadShift = await ApiService.downloadShift();
                              var downloadBudget = await ApiService.downloadBudget();
                              var downloadRefueling = await ApiService.downloadRefueling();

                              for(int i=0; i < downloadEquipment.length; i++){
                              var msEquipment = new MsEquipment(
                                equipment_id : downloadEquipment[i]['equipment_id'] ?? '',
                                manufacturer : downloadEquipment[i]['manufacturer'] ?? '',
                                model_number : downloadEquipment[i]['model_number'] ?? '',
                                tank_capacity : downloadEquipment[i]['tank_capacity'] ?? '',
                                category : downloadEquipment[i]['category'],
                                category_desc : downloadEquipment[i]['category_desc'],
                                auth_group : downloadEquipment[i]['auth_group'],
                                auth_text : downloadEquipment[i]['auth_text'],
                                company_code : downloadEquipment[i]['company_code'],
                                changed_by_system : downloadEquipment[i]['changed_by_system'],
                                createdBy : downloadEquipment[i]['createdBy']?? '',
                                created_at : downloadEquipment[i]['created_at'],
                                updatedBy : downloadEquipment[i]['updatedBy'],
                                updated_at : downloadEquipment[i]['updated_at'],
                                load_capacity : downloadEquipment[i]['load_capacity'] ?? '',
                                load_capacity_unit : downloadEquipment[i]['load_capacity_unit'] ?? '',
                                planner_group : downloadEquipment[i]['planner_group'] ?? '',
                                main_work_center : downloadEquipment[i]['main_work_center'] ?? '',
                              );
                              var testing = await FmsDatabase.instance.createEquipment(msEquipment).then((value) {
                                setState(() {
                                  Global.equipmentStatusDownloaded =  'Berhasil';
                                });
                              } ).onError((error, stackTrace) {
                                setState(() {
                                  Global.equipmentStatusDownloaded = 'Gagal';
                                });
                              });
                              };

                              for(int i=0; i < downloadEmployee.length; i++){
                                var msEmployee = new MsEmployee(
                                  EmployeeID : downloadEmployee[i]['EmployeeID'] ?? '',
                                  EmployeeName : downloadEmployee[i]['EmployeeName'] ?? '',
                                  EmployeeAddress : downloadEmployee[i]['EmployeeAddress'] ?? '',
                                  EmployeePhone: downloadEmployee[i]['EmployeePhone'] ?? '',
                                  positionID: (downloadEmployee[i]['positionID']).toString() ,
                                  SiteId: downloadEmployee[i]['SiteId'] ?? '',
                                  DeptID: (downloadEmployee[i]['DeptID']).toString() ,
                                  CreatedBy: downloadEmployee[i]['CreatedBy'] ?? '',
                                  UpdatedBy: downloadEmployee[i]['UpdatedBy'] ?? '',
                                  created_at: downloadEmployee[i]['created_at'] ?? '',
                                  updated_at: downloadEmployee[i]['updated_at'] ?? '',
                                  EMAIL_ADDRESS: downloadEmployee[i]['EMAIL_ADDRESS'] ?? '',
                                );
                                var testing = await FmsDatabase.instance.createEmployee(msEmployee)
                                    .then((value) {
                                  setState(() {
                                    Global.employeeStatusDownloaded =  'Berhasil';
                                  });
                                } ).onError((error, stackTrace) {
                                  setState(() {
                                    Global.employeeStatusDownloaded = 'Gagal';
                                  });
                                });
                              };

                              for(int i=0; i < downloadStorage.length; i++){
                                var msStorage = new MsStorage(
                                  storageId : (downloadStorage[i]['storageId']).toString() ,
                                  unitId : downloadStorage[i]['unitId'] ?? '',
                                  storageCode : downloadStorage[i]['storageCode'] ?? '',
                                  sLocDescription : downloadStorage[i]['sLocDescription'] ?? '',
                                  capacity : (downloadStorage[i]['capacity'])  ?? '',
                                  isAudited : (downloadStorage[i]['isAudited'])  ?? '',
                                  isActive : (downloadStorage[i]['isActive']) ?? '' ,
                                  dateTime : downloadStorage[i]['dateTime'] ?? '',
                                  createdBy : downloadStorage[i]['createdBy'] ?? '',
                                  createdTime : downloadStorage[i]['createdTime'] ?? '',
                                  modifiedBy : downloadStorage[i]['modifiedBy'] ?? '',
                                  modifiedTime : downloadStorage[i]['modifiedTime'] ?? '',
                                  siteID : downloadStorage[i]['siteID'] ?? '',
                                );
                                var testing = await FmsDatabase.instance.createStorage(msStorage)
                                .then((value) {
                                  setState(() {
                                    Global.storageStatusDownloaded =  'Berhasil';
                                  });
                                } ).onError((error, stackTrace) {
                                  setState(() {
                                    Global.storageStatusDownloaded = 'Gagal';
                                  });
                                });
                              };

                              for(int i=0; i < downloadShift.length; i++){
                                var msShift = new MsShift(
                                  ShiftId : downloadShift[i]['shift_id'] ?? '' ,
                                  ShiftName : downloadShift[i]['ShiftName'] ?? '',
                                  siteId : downloadShift[i]['site_id'] ?? '',
                                  ShiftStartTime : downloadShift[i]['ShiftStartTime'] ?? '',
                                  ShiftEndTime : downloadShift[i]['ShiftEndTime'] ?? '',
                                );
                                var testing = await FmsDatabase.instance.createShift(msShift)
                                    .then((value) {
                                  setState(() {
                                    Global.shiftStatusDownloaded =  'Berhasil';
                                  });
                                } ).onError((error, stackTrace) {
                                  setState(() {
                                    Global.shiftStatusDownloaded = 'Gagal';
                                  });
                                });
                              };

                              for(int i=0; i < downloadBudget!.data!.length; i++){
                                var msBudget = new Datum(
                                  budgetId :  downloadBudget.data![i].budgetId  ,
                                  equipmentId : downloadBudget.data![i].equipmentId ,
                                  siteId : downloadBudget.data![i].siteId  ,
                                  fuelConsumption : downloadBudget.data![i].fuelConsumption  ,
                                  modelNumber : downloadBudget.data![i].modelNumber  ,
                                );
                                var testing = await FmsDatabase.instance.createBudget(msBudget)
                                    .then((value) {
                                  setState(() {
                                    Global.budgetStatusDownloaded =  'Berhasil';
                                  });
                                } ).onError((error, stackTrace) {
                                  setState(() {
                                    Global.budgetStatusDownloaded = 'Gagal';
                                  });
                                });
                              };

                              for(int i=0; i < downloadRefueling!.data!.length; i++){
                                var refueling = new Refuel(
                                  refuelingId :  downloadRefueling.data![i].refuelingId,
                                  statusHm :  downloadRefueling.data![i].statusHm,
                                  unitCode :  downloadRefueling.data![i].unitCode,
                                  unitType :  downloadRefueling.data![i].unitType,
                                  hmInput :  downloadRefueling.data![i].hmInput,
                                  namaOperator :  downloadRefueling.data![i].namaOperator,
                                  totalisatorBegin :  downloadRefueling.data![i].totalisatorBegin,
                                  totalisatorEnd :  downloadRefueling.data![i].totalisatorEnd,
                                  fuelConsumption :  downloadRefueling.data![i].fuelConsumption,
                                  budget :  downloadRefueling.data![i].budget,
                                  status :  downloadRefueling.data![i].status,
                                  photoMeterFuel :  downloadRefueling.data![i].photoMeterFuel,
                                  photoHmUnit :  downloadRefueling.data![i].photoHmUnit,
                                  shiftId :  downloadRefueling.data![i].shiftId,
                                  siteId :  downloadRefueling.data![i].siteId,
                                  createdBy :  downloadRefueling.data![i].createdBy,
                                  createdAt :  downloadRefueling.data![i].createdAt
                                );
                                var testing = await FmsDatabase.instance.createLastRefueling(refueling)
                                    .then((value) {
                                  setState(() {
                                    Global.refuelingStatusDownloaded =  'Berhasil';
                                  });
                                } ).onError((error, stackTrace) {
                                  setState(() {
                                    Global.refuelingStatusDownloaded = 'Gagal';
                                  });
                                });
                              };

                              if( Global.equipmentStatusDownloaded.isNotEmpty && Global.storageStatusDownloaded.isNotEmpty
                                  && Global.employeeStatusDownloaded.isNotEmpty && Global.shiftStatusDownloaded.isNotEmpty
                                  && Global.budgetStatusDownloaded.isNotEmpty && Global.refuelingStatusDownloaded.isNotEmpty)
                              {
                                LoadingBar.hideLoadingDialog(context);
                                _dialogDownloadAlert();
                              }else if( Global.equipmentStatusDownloaded == 'Gagal'&& Global.storageStatusDownloaded == 'Gagal'
                                  && Global.employeeStatusDownloaded == 'Gagal' && Global.shiftStatusDownloaded == 'Gagal'
                                  && Global.budgetStatusDownloaded == 'Gagal' && Global.refuelingStatusDownloaded == 'Gagal')
                              {
                                LoadingBar.hideLoadingDialog(context);
                                _dialogFailAlert();
                              }

                            },
                            child: Text('Download All',
                                style: TextStyle(
                                    color: Color(0xffF0C419),
                                    fontFamily: Fonts.REGULAR,
                                    fontSize: 18)),
                          ),
                        ],
                      ),
                    ),
                    SingleChildScrollView(
                      child: Wrap(
                          direction: Axis.horizontal,
                          spacing: 0,
                          children: [
                            Container(height:100,
                            margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                             child: Card(
                                elevation: 0.8,
                                shadowColor: Colors.grey,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Padding(
                                      padding: const EdgeInsets.only(left: 0),
                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment.center,
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                              'Data MsEquipment',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                          Text(
                                              Global.time,
                                              textAlign: TextAlign.center,
                                              style: TextStyle(color: Colors.grey,
                                                  fontFamily: Fonts.REGULAR,fontSize: 12)
                                          ),
                                        ],
                                      ),
                                    ),
                                    Global.equipmentStatusDownloaded == 'Berhasil' ?
                                    Icon(
                                      Icons.check_circle,
                                      color: Coloring.mainColor,
                                      size: 20,
                                    ) : Global.equipmentStatusDownloaded == 'Gagal' ?
                                    Icon(
                                      Icons.cancel,
                                      color: Colors.red,
                                      size: 20,
                                    ) :
                                    Icon(
                                      Icons.sync,
                                      color: Colors.grey,
                                      size: 20,
                                    )
                                  ],
                                )
                            ),
                              ),
                            Container(height:100,
                              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                              child: Card(
                                  elevation: 0.8,
                                  shadowColor: Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'Data MsEmployee',
                                                textAlign: TextAlign.left,
                                                style: TextStyle(color: Colors.black,
                                                    fontFamily: Fonts.REGULAR,fontSize: 18)
                                            ),
                                            Text(
                                                Global.time,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.grey,
                                                    fontFamily: Fonts.REGULAR,fontSize: 12)
                                            ),
                                          ],
                                        ),
                                      ),
                                      Global.employeeStatusDownloaded == 'Berhasil' ?
                                      Icon(
                                        Icons.check_circle,
                                        color: Coloring.mainColor,
                                        size: 20,
                                      ) : Global.employeeStatusDownloaded == 'Gagal' ?
                                      Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 20,
                                      ) :
                                      Icon(
                                        Icons.sync,
                                        color: Colors.grey,
                                        size: 20,
                                      )
                                    ],
                                  )
                              ),
                            ),
                            Container(height:100,
                              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                              child: Card(
                                  elevation: 0.8,
                                  shadowColor: Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'Data MsStorages',
                                                textAlign: TextAlign.left,
                                                style: TextStyle(color: Colors.black,
                                                    fontFamily: Fonts.REGULAR,fontSize: 18)
                                            ),
                                            Text(
                                                Global.time,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.grey,
                                                    fontFamily: Fonts.REGULAR,fontSize: 12)
                                            ),
                                          ],
                                        ),
                                      ),
                                      Global.storageStatusDownloaded == 'Berhasil' ?
                                      Icon(
                                        Icons.check_circle,
                                        color: Coloring.mainColor,
                                        size: 20,
                                      ) : Global.storageStatusDownloaded == 'Gagal' ?
                                      Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 20,
                                      ) :
                                      Icon(
                                        Icons.sync,
                                        color: Colors.grey,
                                        size: 20,
                                      )
                                    ],
                                  )
                              ),
                            ),
                            Container(height:100,
                              margin: EdgeInsets.symmetric(horizontal: 10,vertical: 10),
                              child: Card(
                                  elevation: 0.8,
                                  shadowColor: Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'Data MsShift',
                                                textAlign: TextAlign.left,
                                                style: TextStyle(color: Colors.black,
                                                    fontFamily: Fonts.REGULAR,fontSize: 18)
                                            ),
                                            Text(
                                                Global.time,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.grey,
                                                    fontFamily: Fonts.REGULAR,fontSize: 12)
                                            ),
                                          ],
                                        ),
                                      ),
                                      Global.shiftStatusDownloaded == 'Berhasil' ?
                                      Icon(
                                        Icons.check_circle,
                                        color: Coloring.mainColor,
                                        size: 20,
                                      ) : Global.shiftStatusDownloaded == 'Gagal' ?
                                      Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 20,
                                      ) :
                                      Icon(
                                        Icons.sync,
                                        color: Colors.grey,
                                        size: 20,
                                      )
                                    ],
                                  )
                              ),
                            ),
                            Container(height:100,
                              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                              child: Card(
                                  elevation: 0.8,
                                  shadowColor: Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'Data MsBudget',
                                                textAlign: TextAlign.left,
                                                style: TextStyle(color: Colors.black,
                                                    fontFamily: Fonts.REGULAR,fontSize: 18)
                                            ),
                                            Text(
                                                Global.time,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.grey,
                                                    fontFamily: Fonts.REGULAR,fontSize: 12)
                                            ),
                                          ],
                                        ),
                                      ),
                                      Global.budgetStatusDownloaded == 'Berhasil' ?
                                      Icon(
                                        Icons.check_circle,
                                        color: Coloring.mainColor,
                                        size: 20,
                                      ) : Global.budgetStatusDownloaded == 'Gagal' ?
                                      Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 20,
                                      ) :
                                      Icon(
                                        Icons.sync,
                                        color: Colors.grey,
                                        size: 20,
                                      )
                                    ],
                                  )
                              ),
                            ),
                            Container(height:100,
                              margin: EdgeInsets.symmetric(horizontal: 20,vertical: 10),
                              child: Card(
                                  elevation: 0.8,
                                  shadowColor: Colors.grey,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(0),
                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Text(
                                                'Data Last Refueling',
                                                textAlign: TextAlign.left,
                                                style: TextStyle(color: Colors.black,
                                                    fontFamily: Fonts.REGULAR,fontSize: 18)
                                            ),
                                            Text(
                                                Global.time,
                                                textAlign: TextAlign.center,
                                                style: TextStyle(color: Colors.grey,
                                                    fontFamily: Fonts.REGULAR,fontSize: 12)
                                            ),
                                          ],
                                        ),
                                      ),
                                      Global.budgetStatusDownloaded == 'Berhasil' ?
                                      Icon(
                                        Icons.check_circle,
                                        color: Coloring.mainColor,
                                        size: 20,
                                      ) : Global.budgetStatusDownloaded == 'Gagal' ?
                                      Icon(
                                        Icons.cancel,
                                        color: Colors.red,
                                        size: 20,
                                      ) :
                                      Icon(
                                        Icons.sync,
                                        color: Colors.grey,
                                        size: 20,
                                      )
                                    ],
                                  )
                              ),
                            ),
                          ],
                        ),
                    )
                  ],
                ),
              ),
              Refresh(),
            ],
          ),
        ),
      ),
    );
  }

  ///pop up status
  Future<void> _dialogDownloadAlert() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Berhasil!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Berhasil download data"),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Oke'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

  ///pop up status
  Future<void> _dialogFailAlert() async {
    return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: const Text('Oops!'),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text("Gagal upload data. Periksa jaringan Anda!"),
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: const Text('Oke'),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
          ],
        );
      },
    );
  }

}

class successDownloaded extends StatelessWidget {
  const successDownloaded({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return  Icon(
      Icons.check_box,
      color: Coloring.mainColor,
      size: 30,
    );
  }
}







