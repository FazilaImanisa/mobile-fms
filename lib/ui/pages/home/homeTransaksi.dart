part of '../pages.dart';

class homeTransaksi extends StatefulWidget {
  const homeTransaksi({Key? key}) : super(key: key);

  @override
  _homeTransaksiState createState() => _homeTransaksiState();
}

class _homeTransaksiState extends State<homeTransaksi> {
  List<Map> data = [];

  @override
  initState(){
    Global.getHistory().then((value) {
      setState(() {
        data = value;
      });
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            leading: new IconButton(
              icon: new Icon(Icons.arrow_back,color: Colors.black,),
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => homeDashboard()),
                );
              },
            ),
            backgroundColor: Colors.white,
            bottom: const TabBar(
              physics: NeverScrollableScrollPhysics(),
              indicatorColor: Coloring.mainColor,
              tabs: [
                Tab(
                  child: Text(
                    'Refueling',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Coloring.mainColor,
                        fontFamily: Fonts.REGULAR,fontSize: 12)
                ),),
                Tab( text: ''),
                Tab( text: '')
              ],
            ),

            title: const Text('History',
                style: TextStyle(color: Colors.black,
                    fontFamily: Fonts.REGULAR,fontSize: 18)),
          ),
          body: TabBarView(
            physics: NeverScrollableScrollPhysics(),
            children:  <Widget>[
              // SingleChildScrollView(
              //   scrollDirection: Axis.vertical,
              //   physics: AlwaysScrollableScrollPhysics(),
              //   child: Column(
              //     children: [
              //       FutureBuilder<List>(
              //         future: FmsDatabase.instance.readHistoryRefueling(), // a previously-obtained Future<String> or null
              //         builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              //           List<Widget> children;
              //           if (snapshot.hasData) {
              //             return ListView.builder(
              //                 scrollDirection: Axis.vertical,
              //                 shrinkWrap: true,
              //                 padding: const EdgeInsets.only(top:8,right:8, left:8),
              //                 itemCount: snapshot.data!.length,
              //                 itemBuilder: (BuildContext context, int index) {
              //                   return Card(
              //                     elevation: 0.8,
              //                     shadowColor: Colors.grey,
              //                     shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(10),
              //                     ),
              //                     child: InkWell(
              //                         onTap: () {
              //                           print('Card tapped.');
              //                         },
              //                         child: Row(
              //                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //                           children: [
              //                             Container(
              //                                 height: 70,
              //                                 width: 50,
              //                                 child:
              //                                 new Image.asset('assets/img/truck.png')),
              //                             Column(
              //                               mainAxisAlignment: MainAxisAlignment.start,
              //                               crossAxisAlignment: CrossAxisAlignment.start,
              //                               children: [
              //                                 Text(snapshot.data![index]['unit_code'],
              //                                     textAlign: TextAlign.left,
              //                                     style: TextStyle(
              //                                         color: Colors.black,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 18)),
              //                                 Text(snapshot.data![index]['fuel_consumption'].toString(),
              //                                     textAlign: TextAlign.center,
              //                                     style: TextStyle(
              //                                         color: Colors.grey,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 12)),
              //                                 Text(snapshot.data![index]['created_at'],
              //                                     textAlign: TextAlign.center,
              //                                     style: TextStyle(
              //                                         color: Colors.grey,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 12)),
              //                               ],
              //                             ),
              //                             Container(
              //                               child: RaisedButton(
              //                                 shape: RoundedRectangleBorder(
              //                                     borderRadius: BorderRadius.circular(5),
              //                                     side: BorderSide(
              //                                         color: Coloring.mainColor)),
              //                                 onPressed: () {
              //                                   Navigator.push(
              //                                       context,
              //                                       MaterialPageRoute(
              //                                           builder: (context) => DetailHistoryTransaksi(index:index))
              //                                   );
              //                                 },
              //                                 color: Colors.white,
              //                                 textColor: Colors.white,
              //                                 child: Text("Detail",
              //                                     style: TextStyle(
              //                                         color: Coloring.mainColor,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 14)),
              //                               ),
              //                             ),
              //                           ],
              //                         )),
              //                   );
              //                 });
              //           } else if (snapshot.hasError) {
              //             return Container(
              //               child: Text('Tidak ada data'),
              //             );
              //           } else {
              //             return Container();
              //           }
              //         },
              //       ),
              //       FutureBuilder<List>(
              //         future: FmsDatabase.instance.readRefueling(), // a previously-obtained Future<String> or null
              //         builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
              //           List<Widget> children;
              //           if (snapshot.hasData) {
              //             return ListView.builder(
              //                 scrollDirection: Axis.vertical,
              //                 shrinkWrap: true,
              //                 padding: const EdgeInsets.symmetric(horizontal: 8),
              //                 itemCount: snapshot.data!.length,
              //                 itemBuilder: (BuildContext context, int index) {
              //                   return Card(
              //                     elevation: 0.8,
              //                     shadowColor: Colors.grey,
              //                     shape: RoundedRectangleBorder(
              //                       borderRadius: BorderRadius.circular(10),
              //                     ),
              //                     child: InkWell(
              //                         onTap: () {
              //                           print('Card tapped.');
              //                         },
              //                         child: Row(
              //                           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              //                           children: [
              //                             Container(
              //                                 height: 70,
              //                                 width: 50,
              //                                 child:
              //                                 new Image.asset('assets/img/truck.png')),
              //                             Column(
              //                               mainAxisAlignment: MainAxisAlignment.start,
              //                               crossAxisAlignment: CrossAxisAlignment.start,
              //                               children: [
              //                                 Text(snapshot.data![index]['unit_code'],
              //                                     textAlign: TextAlign.left,
              //                                     style: TextStyle(
              //                                         color: Colors.black,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 18)),
              //                                 Text(snapshot.data![index]['fuel_consumption'].toString(),
              //                                     textAlign: TextAlign.center,
              //                                     style: TextStyle(
              //                                         color: Colors.grey,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 12)),
              //                                 Text(snapshot.data![index]['created_at'],
              //                                     textAlign: TextAlign.center,
              //                                     style: TextStyle(
              //                                         color: Colors.grey,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 12)),
              //                               ],
              //                             ),
              //                             Container(
              //                               child: RaisedButton(
              //                                 shape: RoundedRectangleBorder(
              //                                     borderRadius: BorderRadius.circular(5),
              //                                     side: BorderSide(
              //                                         color: Coloring.mainColor)),
              //                                 onPressed: () {
              //                                   Navigator.push(
              //                                       context,
              //                                       MaterialPageRoute(
              //                                           builder: (context) => homeDetailTransaksi(index:index))
              //
              //                                   );
              //                                 },
              //                                 color: Colors.white,
              //                                 textColor: Colors.white,
              //                                 child: Text("Detail",
              //                                     style: TextStyle(
              //                                         color: Coloring.mainColor,
              //                                         fontFamily: Fonts.REGULAR,
              //                                         fontSize: 14)),
              //                               ),
              //                             ),
              //                           ],
              //                         )),
              //                   );
              //                 });
              //           } else if (snapshot.hasError) {
              //             return Container(
              //               child: Text('Tidak ada data'),
              //             );
              //           } else {
              //             return Container();
              //           }
              //         },
              //       ),
              //     ],
              //   ),
              // ),
              SingleChildScrollView(
                scrollDirection: Axis.vertical,
                physics: AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: [
                    ListView.builder(
                        physics: NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        padding: const EdgeInsets.all(8),
                        itemCount: data.length  ,
                        itemBuilder: (BuildContext context, int index) {
                          return Card(
                            elevation: 0.8,
                            shadowColor: Colors.grey,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10),
                            ),
                            child: InkWell(
                                onTap: () {
                                  print('Card tapped.');
                                },
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    (data[index]['unit_code']).contains("HD")  || (data[index]['unit_code']).contains("DA") ?
                                    Container(
                                        height: 70,
                                        width: 55,
                                        child:
                                        new Image.asset('assets/img/truck.png')) :
                                    (data[index]['unit_code']).contains("EX") ?
                                    Container(
                                        height: 90,
                                        width: 60,
                                        child:
                                        new Image.asset('assets/img/excavator.jpeg')) :
                                    (data[index]['unit_code']).contains("DT") ?
                                    Container(
                                        height: 90,
                                        width: 60,
                                        child:
                                        new Image.asset('assets/img/heavy_dump.jpg')) :
                                    (data[index]['unit_code']).contains("BD") ?
                                    Container(
                                        height: 90,
                                        width: 55,
                                        child:
                                        new Image.asset('assets/img/bulldozer.jpeg')) :
                                    (data[index]['unit_code']).contains("GD") ?
                                    Container(
                                        height: 90,
                                        width: 55,
                                        child:
                                        new Image.asset('assets/img/grader.jpg')) :
                                    Container(
                                        height: 70,
                                        width: 50,
                                        child:
                                        new Image.asset('assets/img/truck.png')),
                                    Column(
                                      mainAxisAlignment: MainAxisAlignment.start,
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        Text(data[index]['unit_code'],
                                            textAlign: TextAlign.left,
                                            style: TextStyle(
                                                color: Colors.black,
                                                fontFamily: Fonts.REGULAR,
                                                fontSize: 18)),
                                        Text(data[index]['fuel_consumption'].toString(),
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontFamily: Fonts.REGULAR,
                                                fontSize: 12)),
                                        Text(data[index]['created_at'],
                                            textAlign: TextAlign.center,
                                            style: TextStyle(
                                                color: Colors.grey,
                                                fontFamily: Fonts.REGULAR,
                                                fontSize: 12)),
                                      ],
                                    ),
                                    Container(
                                      child: RaisedButton(
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(5),
                                            side: BorderSide(
                                                color: Coloring.mainColor)),
                                        onPressed: () {
                                          Navigator.push(
                                              context,
                                              MaterialPageRoute(
                                                  builder: (context) => DetailHistoryTransaksi(index:index))
                                          );
                                        },
                                        color: Colors.white,
                                        textColor: Colors.white,
                                        child: Text("Detail",
                                            style: TextStyle(
                                                color: Coloring.mainColor,
                                                fontFamily: Fonts.REGULAR,
                                                fontSize: 14)),
                                      ),
                                    ),
                                  ],
                                )),
                          );
                        })

                    //   },
                    // ),
                  ],
                ),
              ),
              Icon(null),
              Refresh()
            ],
          ),
        ),
      ),
    );
  }
}
