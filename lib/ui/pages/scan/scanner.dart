part of '../pages.dart';

class Scanner extends StatefulWidget {
  const Scanner({Key? key}) : super(key: key);

  @override
  _ScannerState createState() => _ScannerState();
}

class _ScannerState extends State<Scanner> {
  bool _show = false;
  final _gKey = new GlobalKey<ScaffoldState>();
  final qrKey = new GlobalKey(debugLabel: 'QR');
  final List<bool> isSelected = [
    true,false
  ];
  Barcode? barcode;
  QRViewController? controller;
  List selectedEquipment = [];
  List<Map> data = [];

  @override
  void dispose(){
    controller?.dispose();
    super.dispose();
  }

  @override
  void reassemble() async {
    super.reassemble();
    if (Platform.isAndroid) {
      await controller!.pauseCamera();
    } controller?.resumeCamera();
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // this.valueUnit();
    ApiService().fetchEmployee().then((value) async {
      List dataComputed = [];
      SharedPreferences prefs = await SharedPreferences.getInstance();
      String? barcode = await prefs.getString("barcode");
      dataComputed = value.where((element) => element['equipment_id'] == barcode).toList();
      print(dataComputed);
      setState(() {
        selectedEquipment = dataComputed;
      });
    });
    Global.getHistory().then((value) {
      setState(() {
        data = value;
      });
    });
  }


  @override
  Widget build(BuildContext context) {

    return SafeArea(
      child: Stack(
        children: [
          buildQrView(context),
          FutureBuilder(
            future: Future.value(barcode),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                /// Success
                return dialogSuccess(barcode: barcode!.code);
              } else if (snapshot.hasError) {
                /// Failed
                return dialogFail();
              }
              /// Idle
              return selectedScanner();
            },
          ),
        ],
      ),
    );
  }

  ///QR Scanner View
  Widget buildQrView(BuildContext context) => QRView(
    key: qrKey,
    onQRViewCreated: onQRViewCreated,
    overlay: QrScannerOverlayShape(),
  );

  ///QRCode Result View
  Widget buildResult(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
          height: MediaQuery.of(context).size.height / 1.8,
          child: (barcode != null) ? dialogSuccess_(barcode: barcode!.code) : dialogFail()  ),
          // child: (barcode != null) ? dialogSuccess(barcode: barcode!.code) : dialogFail()  ),

    );

  }

  void onQRViewCreated(QRViewController controller){
    setState(() => this.controller = controller);
    controller.scannedDataStream
    .listen((barcode) {
      setState(() => this.barcode = barcode);
      // print(barcode.code);
      FmsDatabase.instance.readLastRefueling(barcode.code).then((value) {
        if(value.length < 1){
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(builder: (context) => dialogFail()),
          );
        }else{
          // Navigator.pushReplacement(
          //   context,
          //   MaterialPageRoute(builder: (context) => dialogSuccess_(barcode: barcode.code)),
          // );
          dialogSuccess(barcode: barcode.code);
        }
      });
    });
  }


   displayBottomSheet()
  {
    _gKey.currentState?.showBottomSheet((context)
    {
      return Container(
        decoration: BoxDecoration(
          color: Colors.transparent,
            borderRadius: BorderRadius.all(Radius.circular(20))
        ),
        height: MediaQuery.of(context).size.height / 1.7 ,
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: const Text('QR Code Tidak Cocok!',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontFamily: Fonts.BOLD,fontSize: 18)),
              ),
              Container(
                  height: 150,
                  width: 150,
                  child: new Image.asset('assets/img/man1.png')
              ),
              Padding(
                padding: const EdgeInsets.all(30.0),
                child: const Text('Pastikan QR code tidak terhalang noda, dan nyalakan lampu flash handphone Anda',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black, fontFamily: Fonts.REGULAR,fontSize: 14)),
              ),
              Container(
                // margin:  EdgeInsets.only(top: 20),
                height: 60,
                child: ButtonTheme(
                  minWidth: 300,
                  child: Container(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                          side: BorderSide(color: Coloring.mainColor)),
                      onPressed: () => Navigator.pop(context),
                      color: Coloring.mainColor,
                      textColor: Colors.white,
                      child: Text("Coba Lagi",
                          style: TextStyle(color: Colors.white, fontFamily: Fonts.BOLD,fontSize: 18)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      );
    }
    );
  }


}

///selectedButtonScanner
class selectedScanner extends StatelessWidget {
  const selectedScanner({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
            children: [
              Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(
                    margin: EdgeInsets.only(bottom:30),
                    height: MediaQuery.of(context).size.height/6,
                    width: MediaQuery.of(context).size.width/1.2,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                    ),
                  )),
              Align(
                alignment: Alignment.bottomCenter,
                child: Container(
                  width: MediaQuery.of(context).size.width/2,
                  margin: EdgeInsets.only(bottom:75),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(5),
                        side: BorderSide(color: Colors.grey)),
                    onPressed: () {
                      // Navigator.push(
                      //   context,
                      //   MaterialPageRoute(builder: (context) => inputKodeUnit()),
                      // );
                    },
                    color: Colors.white,
                    textColor: Colors.white,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        Icon(
                          Icons.speed,
                          color: Colors.black,
                          size: 30,
                        ),
                        Text("Input Kode Unit",
                            style: TextStyle(color: Colors.black, fontFamily: Fonts.REGULAR,fontSize: 14)),
                      ],
                    ),
                  ),
        ),
              ),

            ],
          );
  }
}

///dialogFail
class dialogFail extends StatelessWidget {
  const dialogFail({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomSheet(
        onClosing: (){},
        builder: (context){
          return Container(
            decoration: BoxDecoration(
                color: Colors.transparent,
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            height: MediaQuery.of(context).size.height / 1.7 ,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: const Text('Unit tidak ditemukan!',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black, fontFamily: Fonts.BOLD,fontSize: 18)),
                  ),
                  Icon(
                    Icons.announcement,
                    color: Colors.grey.withOpacity(0.5),
                    size: 100.0,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(30.0),
                    child: const Text('Pastikan unit terdaftar dalam site Anda',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.black, fontFamily: Fonts.REGULAR,fontSize: 14)),
                  ),
                  Container(
                    // margin:  EdgeInsets.only(top: 20),
                    height: 60,
                    child: ButtonTheme(
                      minWidth: 300,
                      child: Container(
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                              side: BorderSide(color: Coloring.mainColor)),
                          onPressed: () => Navigator.pop(context),
                          color: Coloring.mainColor,
                          textColor: Colors.white,
                          child: Text("Coba Lagi",
                              style: TextStyle(color: Colors.white, fontFamily: Fonts.BOLD,fontSize: 18)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }
    );
  }
}

///dialogSuccess
class dialogSuccess extends StatelessWidget {
  const dialogSuccess({Key? key, required this.barcode}) : super(key: key);
  final String barcode;

  @override
  Widget build(BuildContext context) {

    return BottomSheet(
        onClosing: (){},
        builder: (context){
          return Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            height: MediaQuery.of(context).size.height ,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width / 1.1,
                      height: 100,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color(0xffE4E4E4)
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          FutureBuilder<List>(
                            future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                              List<Widget> children;
                              if (snapshot.hasData) {
                                return
                                  (snapshot.data![0]['unit_code']).contains('HD') || (snapshot.data![0]['unit_code']).contains('DA')?
                                  Container(
                                    margin: EdgeInsets.symmetric(horizontal: 20),
                                      height: 70,
                                      width: 55,
                                      child:
                                      new Image.asset('assets/img/truck.png')) :
                                  (snapshot.data![0]['unit_code']).contains('EX') ?
                                  Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                      height: 90,
                                      width: 60,
                                      child:
                                      new Image.asset('assets/img/excavator.jpeg')) :
                                  (snapshot.data![0]['unit_code']).contains('DT') ?
                                  Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                      height: 90,
                                      width: 60,
                                      child:
                                      new Image.asset('assets/img/heavy_dump.jpg')) :
                                  (snapshot.data![0]['unit_code']).contains('BD') ?
                                  Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                      height: 90,
                                      width: 55,
                                      child:
                                      new Image.asset('assets/img/bulldozer.jpeg')) :
                                  (snapshot.data![0]['unit_code']).contains('GD') ?
                                  Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                      height: 90,
                                      width: 55,
                                      child:
                                      new Image.asset('assets/img/grader.jpg')) :
                                  Container(
                                      margin: EdgeInsets.symmetric(horizontal: 20),
                                      height: 70,
                                      width: 50,
                                      child:
                                      new Image.asset('assets/img/truck.png'));
                              } else if (snapshot.hasError) {
                                return Container(
                                  child: Text('Tidak ada data'),
                                );
                              } else {
                                return Container(
                                    child: Text('Tidak ada data')
                                );
                              }
                            },
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      'Unit Code ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(color: Colors.grey,
                                          fontFamily: Fonts.REGULAR,fontSize: 18)
                                  ),
                                  FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(barcode),
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 40),
                                          child: Text(
                                              (snapshot.data![0]['unit_code']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      'Storage Type ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(color: Colors.grey,
                                          fontFamily: Fonts.REGULAR,fontSize: 18)
                                  ),
                                  FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  (snapshot.data![0]['unit_type']) != '' ?
                                        Padding(
                                          padding: EdgeInsets.only(left: 13),
                                          child: Text(
                                              (snapshot.data![0]['unit_type']).toString()  ,
                                            // 'pong',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        ) :
                                        Padding(
                                          padding: EdgeInsets.only(left: 13),
                                          child: Text(
                                            // (snapshot.data![0]['unit_type']).toString()  ,
                                              'Tidak ada data',
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.grey,
                                                  fontStyle: FontStyle.italic,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                      'Budget ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(color: Colors.grey,
                                          fontFamily: Fonts.REGULAR,fontSize: 18)
                                  ),
                                  FutureBuilder<List>(
                                    // future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                                    future: FmsDatabase.instance.readBudget(barcode), // a previously-obtained Future<String> or null
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 60),
                                          child: Text(
                                              (snapshot.data![0]['fuel_consumption']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Text(
                            'Last Refueling',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: Fonts.REGULAR,
                                fontSize: 18)),
                      ),
                      FutureBuilder<List>(
                        future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                          List<Widget> children;
                          if (snapshot.hasData) {
                            return  Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                  (snapshot.data![0]['created_at']).toString() ,
                                textAlign: TextAlign.left,
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontFamily: Fonts.REGULAR,
                                    fontSize: 14)),
                            );
                          } else if (snapshot.hasError) {
                            return Container(
                              child: Text('Tidak ada data'),
                            );
                          } else {
                            return Container(
                                child: Text('Tidak ada data')
                            );
                          }
                        },
                      ),
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.only(bottom:20),
                      width: MediaQuery.of(context).size.width / 1.1,
                      height: 200,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color(0xffE4E4E4)
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    'Nama operator',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                                SizedBox(height: 10),
                                Text(
                                    'HM Unit',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                                Text(
                                    'Quantity refueling',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                                Text(
                                    'Nama Fuelman',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 0),
                                          child: Text(
                                              (snapshot.data![0]['nama_operator']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ),
                                FutureBuilder<List>(
                                  future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                                  builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                    List<Widget> children;
                                    if (snapshot.hasData) {
                                      return  Padding(
                                        padding: EdgeInsets.only(left: 0),
                                        child: Text(
                                            (snapshot.data![0]['hm_input']).toString()  ,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black,
                                                fontFamily: Fonts.REGULAR,fontSize: 18)
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Container(
                                        child: Text('Tidak ada data'),
                                      );
                                    } else {
                                      return Container(
                                          child: Text('Tidak ada data')
                                      );
                                    }
                                  },
                                ),
                                FutureBuilder<List>(
                                  future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                                  builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                    List<Widget> children;
                                    if (snapshot.hasData) {
                                      return  Padding(
                                        padding: EdgeInsets.only(left: 0),
                                        child: Text(
                                            (snapshot.data![0]['fuel_consumption']).toString()  ,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black,
                                                fontFamily: Fonts.REGULAR,fontSize: 18)
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Container(
                                        child: Text('Tidak ada data'),
                                      );
                                    } else {
                                      return Container(
                                          child: Text('Tidak ada data')
                                      );
                                    }
                                  },
                                ),
                                FutureBuilder<List>(
                                  future: FmsDatabase.instance.readLastRefueling(barcode), // a previously-obtained Future<String> or null
                                  builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                    List<Widget> children;
                                    if (snapshot.hasData) {
                                      return  Padding(
                                        padding: EdgeInsets.only(left: 0),
                                        child: Text(
                                            (snapshot.data![0]['created_by']).toString()  ,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black,
                                                fontFamily: Fonts.REGULAR,fontSize: 18)
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Container(
                                        child: Text('Tidak ada data'),
                                      );
                                    } else {
                                      return Container(
                                          child: Text('Tidak ada data')
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.1,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.all(Radius.circular(20))
                    ),
                    child: ButtonTheme(
                      minWidth: 150,
                      child: Container(
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                              side: BorderSide(color: Coloring.mainColor)),
                          onPressed: () async {
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            await prefs.setString('barcode','${barcode}' );
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => scanRefueling(barcode_id: barcode,)),
                            );
                          },
                          color: Coloring.mainColor,
                          textColor: Colors.white,
                          child: Text("Selanjutnya",
                              style: TextStyle(color: Colors.white, fontFamily: Fonts.REGULAR,fontSize: 24)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),);

        }
    );
  }
}

///inputKodeUnit
class inputKodeUnit extends StatelessWidget {
  const inputKodeUnit({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20,vertical: 50),
                width: MediaQuery.of(context).size.width ,
                decoration:
                BoxDecoration(borderRadius: BorderRadius.circular(20)),
                child: TextFormField(
                  autofocus: false,
                  decoration: InputDecoration(
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(
                        color: Colors.grey.shade200,
                      ),
                    ),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(20),
                      borderSide: BorderSide(
                        color: Colors.white,
                      ),
                    ),
                    fillColor: Color(0xffFFFFFF),
                    filled: true,
                    contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
                  ),
                  onChanged: (value)=> {
                    // Refueling.fEquipmentId = value
                  }, //dummy value
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width / 1.1,
                height: 60,
                decoration: BoxDecoration(
                    color: Colors.transparent,
                    borderRadius: BorderRadius.all(Radius.circular(20))
                ),
                child: ButtonTheme(
                  minWidth: 150,
                  child: Container(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                          side: BorderSide(color: Coloring.mainColor)),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) => scanRefueling(barcode_id: '',)),
                        );
                      },
                      color: Coloring.mainColor,
                      textColor: Colors.white,
                      child: Text("Submit",
                          style: TextStyle(color: Colors.white, fontFamily: Fonts.REGULAR,fontSize: 24)),
                    ),
                  ),
                ),
              ),
            ],
          ),
    );
  }
}

class dialogSuccess_ extends StatefulWidget {
  const dialogSuccess_({Key? key, required this.barcode}) : super(key: key);
  final String barcode;

  @override
  _dialogSuccess_State createState() => _dialogSuccess_State();
}

class _dialogSuccess_State extends State<dialogSuccess_> {
  @override
  void initState() {
    super.initState();
    this.valueUnit();
  }

  @override
  valueUnit() async {
    return await FmsDatabase.instance.readHistoryRefueling().then((value) => print(value));
  }

  @override
  Widget build(BuildContext context) {

    return BottomSheet(
        onClosing: (){},
        builder: (context){
          return Container(
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))
            ),
            height: MediaQuery.of(context).size.height ,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Container(
                      width: MediaQuery.of(context).size.width / 1.1,
                      height: 100,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color(0xffE4E4E4)
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          FutureBuilder<List>(
                            future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                            builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                              List<Widget> children;
                              if (snapshot.hasData) {
                                return
                                  (snapshot.data![0]['unit_code']).contains('HD') || (snapshot.data![0]['unit_code']).contains('DA')?
                                  Container(
                                      height: 70,
                                      width: 55,
                                      child:
                                      new Image.asset('assets/img/hpu_logo.png')) :
                                  (snapshot.data![0]['unit_code']).contains('EX') ?
                                  Container(
                                      height: 90,
                                      width: 60,
                                      child:
                                      new Image.asset('assets/img/excavator.jpeg')) :
                                  (snapshot.data![0]['unit_code']).contains('DT') ?
                                  Container(
                                      height: 90,
                                      width: 60,
                                      child:
                                      new Image.asset('assets/img/heavy_dump.jpg')) :
                                  (snapshot.data![0]['unit_code']).contains('BD') ?
                                  Container(
                                      height: 90,
                                      width: 55,
                                      child:
                                      new Image.asset('assets/img/bulldozer.jpeg')) :
                                  (snapshot.data![0]['unit_code']).contains('GD') ?
                                  Container(
                                      height: 90,
                                      width: 55,
                                      child:
                                      new Image.asset('assets/img/grader.jpg')) :
                                  Container(
                                      height: 70,
                                      width: 50,
                                      child:
                                      new Image.asset('assets/img/hpu_logo.png'));
                              } else if (snapshot.hasError) {
                                return Container(
                                  child: Text('Tidak ada data'),
                                );
                              } else {
                                return Container(
                                    child: Text('Tidak ada data')
                                );
                              }
                            },
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      'Unit Code ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(color: Colors.grey,
                                          fontFamily: Fonts.REGULAR,fontSize: 18)
                                  ),
                                  FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(widget.barcode),
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 20),
                                          child: Text(
                                              (snapshot.data![0]['unit_code']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                      'Storage Type ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(color: Colors.grey,
                                          fontFamily: Fonts.REGULAR,fontSize: 18)
                                  ),
                                  FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 23),
                                          child: Text(
                                              (snapshot.data![0]['unit_type']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ],
                              ),
                              Row(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Text(
                                      'Budget ',
                                      textAlign: TextAlign.left,
                                      style: TextStyle(color: Colors.grey,
                                          fontFamily: Fonts.REGULAR,fontSize: 18)
                                  ),
                                  FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 40),
                                          child: Text(
                                              (snapshot.data![0]['budget']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      )
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: [
                      Padding(
                        padding: EdgeInsets.only(top: 20),
                        child: Text(
                            'Last Refueling',
                            textAlign: TextAlign.left,
                            style: TextStyle(
                                color: Colors.black,
                                fontFamily: Fonts.REGULAR,
                                fontSize: 18)),
                      ),
                      FutureBuilder<List>(
                        future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                        builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                          List<Widget> children;
                          if (snapshot.hasData) {
                            return  Padding(
                              padding: EdgeInsets.symmetric(vertical: 10),
                              child: Text(
                                  (snapshot.data![0]['created_at']).toString() ,
                                  textAlign: TextAlign.left,
                                  style: TextStyle(
                                      color: Colors.grey,
                                      fontFamily: Fonts.REGULAR,
                                      fontSize: 14)),
                            );
                          } else if (snapshot.hasError) {
                            return Container(
                              child: Text('Tidak ada data'),
                            );
                          } else {
                            return Container(
                                child: Text('Tidak ada data')
                            );
                          }
                        },
                      ),
                    ],
                  ),
                  Container(
                      margin: EdgeInsets.only(bottom:20),
                      width: MediaQuery.of(context).size.width / 1.1,
                      height: 200,
                      decoration: BoxDecoration(
                          border: Border.all(
                              color: Color(0xffE4E4E4)
                          ),
                          color: Colors.white,
                          borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                      child: Padding(
                        padding: const EdgeInsets.only(left: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    'Nama operator',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                                SizedBox(height: 10),
                                Text(
                                    'HM Unit',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                                Text(
                                    'Quantity refueling',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                                Text(
                                    'Nama Fuelman',
                                    textAlign: TextAlign.left,
                                    style: TextStyle(color: Colors.grey,
                                        fontFamily: Fonts.REGULAR,fontSize: 18)
                                ),
                              ],
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: MediaQuery.of(context).size.width / 2,
                                  margin: EdgeInsets.only(bottom: 10),
                                  child: FutureBuilder<List>(
                                    future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                                    builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                      List<Widget> children;
                                      if (snapshot.hasData) {
                                        return  Padding(
                                          padding: EdgeInsets.only(left: 0),
                                          child: Text(
                                              (snapshot.data![0]['nama_operator']).toString()  ,
                                              textAlign: TextAlign.left,
                                              style: TextStyle(color: Colors.black,
                                                  fontFamily: Fonts.REGULAR,fontSize: 18)
                                          ),
                                        );
                                      } else if (snapshot.hasError) {
                                        return Container(
                                          child: Text('Tidak ada data'),
                                        );
                                      } else {
                                        return Container(
                                            child: Text('Tidak ada data')
                                        );
                                      }
                                    },
                                  ),
                                ),
                                FutureBuilder<List>(
                                  future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                                  builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                    List<Widget> children;
                                    if (snapshot.hasData) {
                                      return  Padding(
                                        padding: EdgeInsets.only(left: 0),
                                        child: Text(
                                            (snapshot.data![0]['hm_input']).toString()  ,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black,
                                                fontFamily: Fonts.REGULAR,fontSize: 18)
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Container(
                                        child: Text('Tidak ada data'),
                                      );
                                    } else {
                                      return Container(
                                          child: Text('Tidak ada data')
                                      );
                                    }
                                  },
                                ),
                                FutureBuilder<List>(
                                  future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                                  builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                    List<Widget> children;
                                    if (snapshot.hasData) {
                                      return  Padding(
                                        padding: EdgeInsets.only(left: 0),
                                        child: Text(
                                            (snapshot.data![0]['fuel_consumption']).toString()  ,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black,
                                                fontFamily: Fonts.REGULAR,fontSize: 18)
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Container(
                                        child: Text('Tidak ada data'),
                                      );
                                    } else {
                                      return Container(
                                          child: Text('Tidak ada data')
                                      );
                                    }
                                  },
                                ),
                                FutureBuilder<List>(
                                  future: FmsDatabase.instance.readLastRefueling(widget.barcode), // a previously-obtained Future<String> or null
                                  builder: (BuildContext context, AsyncSnapshot<List> snapshot) {
                                    List<Widget> children;
                                    if (snapshot.hasData) {
                                      return  Padding(
                                        padding: EdgeInsets.only(left: 0),
                                        child: Text(
                                            (snapshot.data![0]['created_by']).toString()  ,
                                            textAlign: TextAlign.left,
                                            style: TextStyle(color: Colors.black,
                                                fontFamily: Fonts.REGULAR,fontSize: 18)
                                        ),
                                      );
                                    } else if (snapshot.hasError) {
                                      return Container(
                                        child: Text('Tidak ada data'),
                                      );
                                    } else {
                                      return Container(
                                          child: Text('Tidak ada data')
                                      );
                                    }
                                  },
                                ),
                              ],
                            ),
                          ],
                        ),
                      )
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width / 1.1,
                    height: 60,
                    decoration: BoxDecoration(
                        color: Colors.transparent,
                        borderRadius: BorderRadius.all(Radius.circular(20))
                    ),
                    child: ButtonTheme(
                      minWidth: 150,
                      child: Container(
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                              side: BorderSide(color: Coloring.mainColor)),
                          onPressed: () async {
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            await prefs.setString('barcode','${widget.barcode}' );
                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => scanRefueling(barcode_id: widget.barcode,)),
                            );
                          },
                          color: Coloring.mainColor,
                          textColor: Colors.white,
                          child: Text("Selanjutnya",
                              style: TextStyle(color: Colors.white, fontFamily: Fonts.REGULAR,fontSize: 24)),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),);

        }
    );
  }
}






